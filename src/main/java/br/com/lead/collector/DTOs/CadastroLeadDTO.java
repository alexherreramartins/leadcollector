package br.com.lead.collector.DTOs;

import br.com.lead.collector.models.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CadastroLeadDTO {

    public CadastroLeadDTO() {
    }

    @NotBlank(message = "Nome não deve ser nulo.")
    @NotNull(message = "Nome deve ser preenchido.")
    private String nome;

    @NotNull
    @CPF(message = "CPF é inválido!")
    private String cpf;

    @NotNull
    @Email(message = "E-mail inválido!")
    private String email;

    private String telefone;

    @NotNull
    private List<IdProdutoDTO> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<IdProdutoDTO> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<IdProdutoDTO> produtos) {
        this.produtos = produtos;
    }

    public Lead converterParaLead(){
        Lead lead = new Lead();

        lead.setNome(this.getNome());
        lead.setCpf(this.getCpf());
        lead.setEmail(this.getEmail());
        lead.setTelefone(this.getTelefone());

        return lead;
    }
}
