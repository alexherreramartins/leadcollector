package br.com.lead.collector.DTOs;

public class IdProdutoDTO {

    private int id;

    public IdProdutoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
