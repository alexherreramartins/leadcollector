package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto) {
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> consultarProdutos() {
        return produtoService.consultarProdutos();
    }

    @GetMapping("/{id}")
    public Produto consultarProdutoById(@PathVariable(name = "id") int id) {
        try {
            Produto produto = produtoService.consultarProdutoById(id);
            return produto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id") int id) {
        try {
            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return produtoDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id) {

        try {
            produtoService.deletarProduto(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
