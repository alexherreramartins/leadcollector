package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead cadastrarLead(@RequestBody @Valid CadastroLeadDTO lead) {
        return leadService.salvarLead(lead);
    }

    @GetMapping
    public Iterable<Lead> consultarLeads() {
        return leadService.consultarLeads();
    }

    @GetMapping("/{id}")
    public Lead consultarLeadById(@PathVariable(name = "id") int id) {
        try {
            Lead lead = leadService.consultarLeadById(id);
            return lead;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@RequestBody @Valid Lead lead, @PathVariable(name = "id") int id) {
        try {
            Lead leadDB = leadService.atualizarLead(id, lead);
            return leadDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable(name = "id") int id) {

        try {
            leadService.deletarLead(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalhes")
    public Lead detalhesDeLead(@RequestParam(name = "cpf") String cpf) {
        try {
            return leadService.pesquisarPorCPF(cpf);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

