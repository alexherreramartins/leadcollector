package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;


    public Lead salvarLead(CadastroLeadDTO cadastroLeadDTO) {
        Lead lead = cadastroLeadDTO.converterParaLead();

        List<Integer> idProdutos = new ArrayList<>();

        for (IdProdutoDTO id : cadastroLeadDTO.getProdutos()) {
            idProdutos.add(id.getId());
        }

        lead.setProdutos((List<Produto>) produtoRepository.findAllById(idProdutos));

        lead.setData(LocalDate.now());
        return leadRepository.save(lead);
    }

    public Iterable<Lead> consultarLeads() {
        return leadRepository.findAll();
    }

    public Lead consultarLeadById(int id) {
        Optional<Lead> optionalLead = leadRepository.findById(id);

        if (optionalLead.isPresent()) {
            return optionalLead.get();
        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead) {
        Lead leadDB = consultarLeadById(id);

        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id) {
        if (leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Lead pesquisarPorCPF(String cpf) {

        Lead lead = leadRepository.findByCpf(cpf);
        if (lead != null) {
            return lead;
        } else {
            throw new RuntimeException("CPF não encontrado");
        }
    }
}

