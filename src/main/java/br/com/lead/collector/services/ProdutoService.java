package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> consultarProdutos() {
        return produtoRepository.findAll();
    }

    public Produto consultarProdutoById(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);

        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Produto atualizarProduto(int id, Produto produto) {
        Produto produtoDB = consultarProdutoById(id);

        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

}

