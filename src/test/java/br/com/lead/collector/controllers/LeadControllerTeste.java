package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.filter.OncePerRequestFilter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    CadastroLeadDTO cadastroLeadDTO;
    @BeforeEach
    private void setUp(){
        this.lead = new Lead();

        this.lead.setData(LocalDate.now());
        this.lead.setTelefone("43123441");
        this.lead.setEmail("alex@alex");
        this.lead.setCpf("43110841886");
        this.lead.setId(1);
        this.lead.setNome("Alex");

        this.produto = new Produto();
        this.produto.setDescricao("Teste");
        this.produto.setId(1);
        this.produto.setNome("VideoGame");
        this.produto.setPreco(1000.00);

        this.produtos = Arrays.asList(this.produto);

        this.lead.setProdutos(produtos);

        this.cadastroLeadDTO = new CadastroLeadDTO();
        cadastroLeadDTO.setTelefone("43243224");
        cadastroLeadDTO.setNome("xablau");
        cadastroLeadDTO.setEmail("aaaa@aaaaa");
        cadastroLeadDTO.setCpf("631.781.770-78");

        List<IdProdutoDTO> idProdutoDTOList = new ArrayList<>();

        cadastroLeadDTO.setProdutos(idProdutoDTOList);

    }

    @Test
    public void testarBuscarPorID() throws Exception {
        Mockito.when(leadService.consultarLeadById(1)).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.consultarLeadById(1)).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBuscarLeads() throws Exception {
        Mockito.when(leadService.consultarLeads()).thenReturn(Arrays.asList(lead));;

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(cadastroLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome",CoreMatchers.containsString("Alex")));

    }

    @Test
    public void testarValidacaoDeCadastroDeLead() throws Exception {

        Mockito.when(leadService.salvarLead(Mockito.any(CadastroLeadDTO.class))).thenReturn(lead);

        cadastroLeadDTO.setCpf(" mdf8gud");
        cadastroLeadDTO.setEmail("wo8aruwoi");

        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(cadastroLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(CadastroLeadDTO.class));
    }
}
