package br.com.lead.collector.services;


import br.com.lead.collector.DTOs.CadastroLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    private Lead lead;
    private Produto produto;

    @BeforeEach
    private void init() {
        this.lead = new Lead();

        this.lead.setData(LocalDate.now());
        this.lead.setTelefone("43123441");
        this.lead.setEmail("alex@alex");
        this.lead.setCpf("43110841886");
        this.lead.setNome("Nome");
        this.lead.setId(1);

        this.produto = new Produto();
        this.produto.setDescricao("Teste");
        this.produto.setId(1);
        this.produto.setNome("VideoGame");
        this.produto.setPreco(1000.00);

        this.lead.setProdutos(Arrays.asList(this.produto));
    }

    @Test
    public void testarBuscarDeLeadPeloID() {
        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(1)).thenReturn(leadOptional);

        Assertions.assertEquals(lead, leadService.consultarLeadById(1));
    }

    @Test
    public void testarBuscarDeLeadPeloIDNaoExistente() {
        Optional<Lead> leadOptional = Optional.empty();

        Mockito.when(leadRepository.findById(1)).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {
            leadService.consultarLeadById(375);
        });
    }


    @Test
    public void testarSalvarLead() {
        CadastroLeadDTO cadastroLeadDTO = new CadastroLeadDTO();

        cadastroLeadDTO.setCpf("431310841866");
        cadastroLeadDTO.setEmail("ahsha@jshdajk.com");
        cadastroLeadDTO.setNome("KKKK");
        cadastroLeadDTO.setTelefone("2323232");

        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(1);

        List<IdProdutoDTO> idProdutoDTOList = Arrays.asList(idProdutoDTO);

        cadastroLeadDTO.setProdutos(idProdutoDTOList);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(this.lead.getProdutos());
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

        Lead teste = leadService.salvarLead(cadastroLeadDTO);

        Assertions.assertEquals("KKKK", teste.getNome());
    }
}

